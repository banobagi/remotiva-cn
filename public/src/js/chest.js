$(document).ready(function(){



	$(window).on("load resize", function () {

		if(navigator.userAgent.match(/Trident\/7\./)) { // if IE
			$('body').on("mousewheel", function () {
			    // remove default behavior
			    event.preventDefault();

			    //scroll without smoothing
			    var wheelDelta = event.wheelDelta;
			    var currentScrollPosition = window.pageYOffset;
			    window.scrollTo(0, currentScrollPosition - wheelDelta);
			});
		}


		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 992) ? true : false;
				return r;
			})
		);
		//모바일 모션 삭제
		if ($('body').attr('data-mobile') == 'true'){
			$('.case__info, .bano__tit, .bano__txt1, .bano__txt2, .bano__subject, .pain').removeClass('js-motion');
			$('.technology__item.nth-1, .technology__item.nth-2, .ban__img-wrap, .point__info-box.nth-1').removeClass('js-motion2');
			$('.technology__item.nth-3, .technology__item.nth-4, .point__info-box.nth-2').removeClass('js-motion3');
		}else{
			$('.case__info, .bano__tit, .bano__txt1, .bano__txt2, .bano__subject, .pain').addClass('js-motion');
			$('.technology__item.nth-1, .technology__item.nth-2, .ban__img-wrap, .point__info-box.nth-1').addClass('js-motion2');
			$('.technology__item.nth-3, .technology__item.nth-4, .point__info-box.nth-2').addClass('js-motion3');
		}
		locationEvent();
		function locationEvent(){
			window.setTimeout(function(){
				$('.visual-top__tit').addClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.visual-top__subject').addClass('animated fadeInUp');
			},300);
			window.setTimeout(function(){
				$('.visual-top__txt').addClass('animated fadeInUp');
			},500);
			window.setTimeout(function(){
				$('.visual-top__box').addClass('is-active');
			},1200);
		}
		function scollEvent(){
			var $el = $('.header, .lnb__link, .global, .btn1');
			secondPos = $('.section').offset().top;
			if($(this).scrollTop() >= secondPos){
			$el.addClass('is-section');
				$(".js-logo").attr('src',function(){return this.src.replace("_off.","_on.")});
			}else{
			$el.removeClass('is-section');
				$(".js-logo").attr('src',function(){return this.src.replace("_on.","_off.")});
				locationEvent();
			}
		}
		$(window).scroll(function() {
			scollEvent();
		});
		var winH = $(window).height();
		$('.chest, .visual-wheel').height(winH);
		var visualH = $('.chest').height();
		//리얼스토리 이미지 맞추기
		scollEvent();
	});

	$(function(){
		$('.step__item:first-child').addClass('is-active').find('.step__body').css('display','block');
		$(".step__link").on("click keypress",function(e){
			e.preventDefault();
			var $target = $(this).parents(".step__item");
			if ((e.keyCode == 13)||(e.type == 'click'))$(this).parents('.step__item').toggleClass("is-active").siblings().removeClass("is-active");
			if ( $target.is('.is-active')){
				$(this).parent().next().slideDown('500');
				$target.siblings().find('.step__body').slideUp('500');
			} else {
				$(this).parent().next().slideUp('500');
			}
		});
	});


	$('.js-scanner').cycle({
		slides:'.scanner__item',
		fx:'scrollHorz',
		loop:false,
		timeout:0,
		prev:'.js-scanner-prev',
		next:'.js-scanner-next',
		swipe:true,
		maxZ:50
	});
	$('.js-healing').cycle({
		slides:'.healing__item',
		fx:'scrollHorz',
		loop:false,
		timeout:0,
		prev:'.js-healing-prev',
		next:'.js-healing-next',
		swipe:true,
		maxZ:50
	});
	//다음
	$('.js-healing').on('cycle-next cycle-prev',function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag){
		var healNum = $('.healing__item.cycle-slide-active').attr('data-num');
		if(healNum == 1){
			$('.healing__num').html('Type 01');
			$('.healing__tit').html('发红或褐色的疤痕');
			$('.healing__txt').html('通过V-beam激光仪，镭射激光柔肤来治疗，普通间隔3周，治疗3-5次');
		}else if(healNum == 2){
			$('.healing__num').html('Type 02');
			$('.healing__tit').html('微小疤痕');
			$('.healing__txt').html('通过点阵激光间隔4-6周进行3次左右治疗后再用上皮再生因子和再生胶带来做治疗');
		}else if(healNum == 3){
			$('.healing__num').html('Type 03');
			$('.healing__tit').html('突出来的疤痕');
			$('.healing__txt').html('注射疗法（混合肉毒素和曲安奈德），4个星期一次，进行3-5次治疗');
		}
	});
});
