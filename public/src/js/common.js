$(document).ready(function(){
	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1025) ? true : false;
				return r;
			})
		);
	});
	var cnt = 1
	var posY;
	$('.js-menu').on('click',function(e){
		e.preventDefault();
		$(this).toggleClass('is-active');
		$('.menu, .global').toggleClass('is-active');
		$('.btn1').toggleClass('is-important');
		$('.dim').fadeToggle()
		if ($('body').attr('data-mobile') == 'true'){
			if(cnt == 1){
				posY = $(window).scrollTop();
				$('html, body').addClass("is-active");
				$('.common').css("top",-posY);
				return cnt = 0;

			}else{
				$('.common').css("top",'0');
				$('html, body').removeClass("is-active");
				posY = $(window).scrollTop(posY);
				return cnt = 1;
			}
		}else{
			if(cnt == 1){
				$('body').addClass('is-hidden');
				return cnt = 0;
			}else{
				$('body').removeClass('is-hidden');
				return cnt = 1;
			}
		}
	});
	$('.js-popup-open').on('click',function(e){
		e.preventDefault();
		if ($('body').attr('data-mobile') == 'true'){
			if(cnt == 1){
				posY = $(window).scrollTop();
				$('.common').css("top",-posY);
				$('html, body').addClass("is-active");

			}
		}
		$('body').addClass('is-hidden');
		$('.popup-c').fadeIn(300);
	});
	$('.js-popup-close').on('click',function(e){
		e.preventDefault();
		$('.popup-c').fadeOut(300);
		$('body').removeClass('is-hidden');
		if(cnt == 0){
			return false;
		}
		if ($('body').attr('data-mobile') == 'true'){
			$('.common').css("top",'0');
			$('html, body').removeClass("is-active");
			posY = $(window).scrollTop(posY);
		}

	});
	$('.popup-c__link').on('click',function(e){
    	e.preventDefault();
    	var tab_id = $(this).attr('data-tab');
    	$('.popup-c__item').removeClass('is-active');
    	$('.popup-c__content').removeClass('is-active');
    	$(this).parent().addClass('is-active');
    	$(".popup-c__content"+"."+tab_id).addClass('is-active');
    });
	$('.js-terms-open').on('click',function(e){
		e.preventDefault();
		$('.terms, .is-close').show();
		$('.popup__check, .is-view').hide();
	});
	$('.js-terms-close').on('click',function(e){
		e.preventDefault();
		$('.terms, .is-close').hide();
		$('.popup__check, .is-view').show();
	});
	$('.js-select1').on('click',function(){
		var selectH = $(this).parent().find('.select1__list').outerHeight();
		$(this).parent().find('.select1__list').css('top',-selectH)
		$(this).parent().toggleClass('is-active');
	});
	$('.js-global').on('click',function(){
		$(this).parent().toggleClass('is-active');
	});
	$('.gnb__depth1-link').on('click',function(e){
		var gnbDepth = $(this).parent().attr('data-depth');
		$(this).parent().toggleClass('is-active').siblings().removeClass('is-active')
		if(gnbDepth == 'true'){e.preventDefault();}
	});

	// gotop button
	$('.js-top').click(function(e){
		e.preventDefault();
		$('html,body').animate({'scrollTop':'0'},300)
	});
	$(window).load(function(){
		$('.js-terms-scroll').mCustomScrollbar({
			theme:"dark"
		});
	});

	//스크롤 모션
	$('.js-visual').on("mousewheel DOMMouseScroll",function(e){

		var delta  = e.originalEvent.wheelDelta || e.originalEvent.detail*-1 || e.originalEvent.deltaY*-1;

		var secondPos = $('.section');
		var fullPos = secondPos.offset().top;
		if(delta >= 0) {

			// $("html,body").stop().animate({scrollTop:0});

		} else if (delta < 0) {

			$("html,body").stop().animate({scrollTop:fullPos});
			return false;

		}
	});
	$('.js-visual-wheel, .map, .tab-v1').on("mousewheel DOMMouseScroll",function(e){

		var delta  = e.originalEvent.wheelDelta || e.originalEvent.detail*-1 || e.originalEvent.deltaY*-1;

		var secondPos = $('.section');
		var fullPos = secondPos.offset().top;
		var scrollPos = $(document).scrollTop();
		if(delta >= 0 && fullPos >= scrollPos) {

			$("html,body").stop().animate({scrollTop:0});

		}
	});



	$(function(){
		$(window).scroll(function() {
			var scrollPos = $(this).scrollTop();
			var gotopbtn = $(".js-top");
			var visualH = $(".js-visual").height()/3;
			var footHeight = $(".footer").height() + $(".section__bottom").height()/2;
			var docHeight = $(document).height() - $(window).height() - footHeight;
			if (scrollPos < visualH) {
				gotopbtn.fadeOut(200);
			} else if (scrollPos >= 0 && scrollPos < docHeight) {
				gotopbtn.removeClass("is-fixed");
				gotopbtn.fadeIn(200);
			} else {
				gotopbtn.addClass("is-fixed");
				gotopbtn.fadeIn(200);
			}
		});
	});

	$('.js-down2').click(function(e){
		e.preventDefault();
		$('html,body').animate({'scrollTop':secondPos},300)
	});


	//스크롤 모션 (real,location)
	$(function(){
		$(".js-motion, .js-motion2, .js-motion3").each(function(){
			var $this = $(this);
			var n = function() {

				if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {

					$this.addClass('on');

					// 윈도우 스크롤 이벤트 함수 n 실행 종료
					// $(window).unbind("scroll", n)
				}else{
					// $this.removeClass('on');
				}
			};


			var b = function() {

				// $this 위치값 계산
				if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {

					// 원본 이미지 교체
					$this.addClass('on');

					// 윈도우 스크롤 이벤트 함수 n 실행 종료
					// $(window).unbind("scroll", n)
				}else{
					$this.removeClass('on');
				}
			};
			// 윈도우 스크롤 이벤트로 함수 n 지속 실행
			$(window).bind("scroll", n)
			// $this 위치값 계산
			$(window).bind("load", b)
		});
	});
	//상담신청 hover
	$('.checkbox02 + label').on('mouseenter',function(){
		if ($('body').attr('data-mobile') == 'false'){
			$(this).addClass('is-hover')
		}
	}).on('mouseleave',function(){
		if ($('body').attr('data-mobile') == 'false'){
			$(this).removeClass('is-hover')
		}
	});

	//실시간 상담 탭 효과
	$(function() {
		$('.js-tab-link').on('click',function(e){
			e.preventDefault();
			var $tab1 = $(this).parents('.tab-v2__item');
			var num = $tab1.index();
			$(this).parents('.tab-v2__item').addClass('is-active').siblings().removeClass('is-active');
			$('.tab-v2__content').eq(num).addClass('is-active').siblings().removeClass('is-active');
		});
	});
});
