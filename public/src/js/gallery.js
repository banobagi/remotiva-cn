
$(document).ready(function(){
	//영상제어


	var prevNum = 0;
	$(document).on("click",".js-video-play",function(e){
		e.preventDefault();
		$('.video__bg, .video__btn').addClass('is-hide');
		if(prevNum == 8) {
			$('.swiper-slide-duplicate').find(".video__iframe").remove();
		}else{
			$('.swiper-slide-duplicate').find(".video__player").append('<div id="youkuplayer" class="video__iframe"></div>');
		}
		onPlayerStart()
	});
	function videoStop(){
		$('.video__bg, .video__btn').removeClass('is-hide');

		var f1 = $(".video__iframe").eq(0);
		if(!f1 || !f1.attr("src") ) return;
		var url = f1.attr("src").split("?")[0];

		var data = {
            method: "unload",
            value: 1
        };
		f1[0].contentWindow.postMessage(JSON.stringify(data), url);

		f1 = $(".video__iframe").eq(1);
		if(!f1 || !f1.attr("src") ) return;
		var url = f1.attr("src").split("?")[0];

		var data = {
            method: "unload",
            value: 1
        };
		f1[0].contentWindow.postMessage(JSON.stringify(data), url);
	}
	var isState = "P";
	var isAuto ="T"
	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 992) ? true : false;
				return r;
			}())
		);
		var winH = $(window).height();
		var winW = $(window).width();
		$('.gallery__section').height(winH);
		firstEvent();
	});
	function firstEvent(){
		window.setTimeout(function(){
			$('.gallery-style1__tit').addClass('animated fadeInUp');
		},100);
		window.setTimeout(function(){
			$('.gallery-style1__subject.nth-1').addClass('animated fadeInUp');
		},300);
		window.setTimeout(function(){
			$('.gallery-style1__subject.nth-2').addClass('animated fadeInUp');
		},500);
		window.setTimeout(function(){
			$('.gallery-style1__txt').addClass('animated fadeInUp');
		},700);
		window.setTimeout(function(){
			$('.gallery-style1').addClass('is-active');
		},1200);
	}
	function timer(){
		$({count: 0}).animate({count: 5000}, {
			duration: 5150,
			easing: 'linear',
			progress: function() {
				var num = 1000;
				var count = Math.ceil(this.count)/num;
				var count_num = Math.floor(count);
				var count_decimal = Math.ceil( (count - count_num)*num );
				count_num = ( '00' + count_num ).slice( -2 );
				count_decimal = ( '000' + count_decimal ).slice( -3 );
				count_decimal = ( count_decimal ).slice( 0,2 );
				$('.timer').text( count_num + ":" + count_decimal );
			}
		});
	}

	function endTimer(){
		$({count: 5000}).animate({count: 5000}, {
			duration: 5150,
			easing: 'linear',
			progress: function() {
				var num = 1000;
				var count = Math.ceil(this.count)/num;
				var count_num = Math.floor(count);
				var count_decimal = Math.ceil( (count - count_num)*num );
				count_num = ( '00' + count_num ).slice( -2 );
				count_decimal = ( '000' + count_decimal ).slice( -3 );
				count_decimal = ( count_decimal ).slice( 0,2 );
				$('.timer').text( "00:00" );
			}
		});
	}
	timer();
	move();

	var gallerySlide = new Swiper('.gallery__list', {
		// hashNavigation: {
		// 	replaceState: true,
		// },
		autoplay: {
			delay: 5000,
			disableOnInteraction:false
		},
		// pagination: {
		// 	el: '.gallery-page',
		// 	type: 'fraction',
		// 	currentClass:'gallery-page__num-current',
		// 	totalClass:'gallery-page__num-total'
		// },
		pagination: {
			el: '.gallery-nav',
			bulletClass:'gallery-nav__bullet',
			bulletActiveClass:'is-active',
			clickable:true,
			renderCustom: function (swiper, current, total) {
			  return current + ' of ' + total;
			}
		},
		watchSlidesProgress: true,
        direction: 'vertical',
        slidesPerView: 1,
        mousewheel: true,
		loop:true
	});
	function move() {
		if( $(".progress" ).hasClass("timeAnimation") ){
			$(".progress").removeClass("timeAnimation");
			$(".progress").removeClass("timeFirstAnimation");
			$(".progress").delay(100).queue(function(next) {
				$(this).addClass("timeAnimation");
				next();
			});
		} else{
			$(".progress").addClass("timeAnimation");
		}
	}
	$('.js-gallery').on('click',function(e){
		e.preventDefault();
		if(isState == "P"){
			isState = "S";
			isAuto = "F";
			endTimer();
			$(".progress").removeClass("timeAnimation");
			gallerySlide.autoplay.stop();
			$(this).addClass('is-active');

		}else{
			isState = "P";
			isAuto = "T"
			timer();
			gallerySlide.autoplay.start();
			move();
			$(this).removeClass('is-active');
		}
	});


	var pageTotalnum = $('.gallery-nav__bullet').length;

	$('.gallery-page__num-total').html(pageTotalnum);
	//페이지 이동시 변화
	gallerySlide.on('transitionStart', function () {
		var pageCurrentnum = $('.gallery-nav__bullet.is-active').index();
		$('.gallery-page__num-current').html(pageCurrentnum+1);
		if(isState == "S"){
			endTimer();
			$(".progress").removeClass("timeAnimation");
		}else{
			timer();
			move();
		}
		var num = $('.swiper-slide-active').attr('data-swiper-slide-index');
		if(num == "0"){
			firstEvent();
			videoStop();
		}else if(num == "1"){
			window.setTimeout(function(){
				$('.gallery-circle').addClass('animated fadeInUp');
			},300);
		}else if(num == "2"){
			window.setTimeout(function(){
				$('.gallery-style2__move.nth-1').addClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.gallery-style2__move.nth-2').addClass('animated fadeInUp');
			},300);
			window.setTimeout(function(){
				$('.gallery-style2__tit').addClass('animated fadeInUp');
			},500);
			window.setTimeout(function(){
				$('.gallery-style2').addClass('is-active');
			},900);
		}else if(num == "3"){
			window.setTimeout(function(){
				$('.gallery-style3__move.nth-1').addClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.gallery-style3__move.nth-2').addClass('animated fadeInUp');
			},300);
			window.setTimeout(function(){
				$('.gallery-style3__move.nth-3').addClass('animated fadeInUp');
			},500);
			window.setTimeout(function(){
				$('.gallery-style3__move.nth-4').addClass('animated fadeInUp');
			},900);
		}else if(num == "4"){
			window.setTimeout(function(){
				$('.gallery-style4__txt').addClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.gallery-style4__tit').addClass('animated fadeInUp');
			},300);
			window.setTimeout(function(){
				$('.gallery-style4__tit').addClass('is-active');
			},700);
		}else if(num == "5"){
			window.setTimeout(function(){
				$('.gallery-style5__tit').addClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.gallery-style5__txt').addClass('animated fadeInUp');
			},300);
			window.setTimeout(function(){
				$('.gallery-style5').addClass('is-active');
			},700);
		}else if(num == "6"){
			window.setTimeout(function(){
				$('.gallery-style6__move.nth-1').addClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.gallery-style6__move.nth-2').addClass('animated fadeInUp');
			},300);
			window.setTimeout(function(){
				$('.gallery-style6__tit').addClass('animated fadeInUp');
			},500);
			window.setTimeout(function(){
				$('.gallery-style6').addClass('is-active');
			},900);
		}else if(num == "7"){
			window.setTimeout(function(){
				$('.gallery-style5__txt').addClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.gallery-style5__tit').addClass('animated fadeInUp');
			},300);
			window.setTimeout(function(){
				$('.gallery-style5').addClass('is-active');
			},700);
		}else if(num == "8"){
			window.setTimeout(function(){
				$('.gallery-style7__tit').addClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.gallery-style7__move.nth-1').addClass('animated fadeInUp');
			},300);
			window.setTimeout(function(){
				$('.gallery-style7__move.nth-2').addClass('animated fadeInUp');
			},500);
			window.setTimeout(function(){
				$('.gallery-style7__subject').addClass('animated fadeInUp');
			},700);
			window.setTimeout(function(){
				$('.gallery-style7__box').addClass('is-active');
			},1100);
			videoStop();
		}else if(num == "9"){
			isState = "S";
			endTimer();
			window.setTimeout(function(){
				$(".progress").removeClass("timeAnimation");
			},100);
			gallerySlide.autoplay.stop();
			$('.js-gallery').addClass('is-active');
		}
	});
	//페이지 이동시 초기화
	gallerySlide.on('slideChange', function () {
		var num = $('.swiper-slide-active').attr('data-swiper-slide-index');

		prevNum = 8;
		if(num == "0"){
			$('.gallery-style1__tit').removeClass('animated fadeInUp');
			$('.gallery-style1__subject').removeClass('animated fadeInUp');
			$('.gallery-style1__txt').removeClass('animated fadeInUp');
			window.setTimeout(function(){
				$('.gallery-style1').removeClass('is-active');
			},300);

			prevNum = 0;
		}else if(num == "1"){
			window.setTimeout(function(){
				$('.gallery-circle').removeClass('animated fadeInUp');
			},300);
		}else if(num == "2"){
			window.setTimeout(function(){
				$('.gallery-style2__tit, .gallery-style2__move').removeClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.gallery-style2').removeClass('is-active');
			},300);
		}else if(num == "3"){
			$('.gallery-style3__move').removeClass('animated fadeInUp');
		}else if(num == "4"){
			$('.gallery-style4__tit, .gallery-style4__txt').removeClass('animated fadeInUp');
			window.setTimeout(function(){
				$('.gallery-style4__tit').removeClass('is-active');
			},300);
		}else if(num == "5"){
			$('.gallery-style5__tit, .gallery-style5__txt').removeClass('animated fadeInUp');
			window.setTimeout(function(){
				$('.gallery-style5').removeClass('is-active');
			},300);
		}else if(num == "6"){
			$('.gallery-style6__move, .gallery-style6__tit').removeClass('animated fadeInUp');
			window.setTimeout(function(){
				$('.gallery-style6').removeClass('is-active');
			},300);
		}else if(num == "7"){
			$('.gallery-style5__tit, .gallery-style5__txt').removeClass('animated fadeInUp');
			window.setTimeout(function(){
				$('.gallery-style5').removeClass('is-active');
			},300);
		}else if(num == "8"){
			$('.gallery-style7__tit, .gallery-style7__move, .gallery-style7__subject').removeClass('animated fadeInUp');
			window.setTimeout(function(){
				$('.gallery-style7__box').removeClass('is-active');
			},300);
		}else if(num =="9"){
			if(isState == "S" && isAuto == "T"){
				isState = "P";
				timer();
				gallerySlide.autoplay.start();
				move();
				$('.js-gallery').removeClass('is-active');
			}
			//영상제어
			videoStop();
		}
	});

});
