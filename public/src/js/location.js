$(document).ready(function(){
	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 992) ? true : false;
				return r;
			}())
		);
		locationEvent();
		function locationEvent(){
			window.setTimeout(function(){
				$('.visual-top__tit').addClass('animated fadeInUp');
			},100);
			window.setTimeout(function(){
				$('.visual-top__subject').addClass('animated fadeInUp');
			},300);
			window.setTimeout(function(){
				$('.visual-top__txt').addClass('animated fadeInUp');
			},500);
			window.setTimeout(function(){
				$('.visual-top__box').addClass('is-active');
			},1200);
		}
		function scollEvent(){
			var $el = $('.header, .lnb__link, .global, .btn1');
			secondPos = $('.section').offset().top;
			if($(this).scrollTop() >= secondPos){
			$el.addClass('is-section');
				$(".js-logo").attr('src',function(){return this.src.replace("_off.","_on.")});
			}else{
			$el.removeClass('is-section');
				$(".js-logo").attr('src',function(){return this.src.replace("_on.","_off.")});
				locationEvent();
			}
		}
		$(window).scroll(function() {
			scollEvent();
		});
		var winH = $(window).height();
		$('.visual-top, .visual-wheel').height(winH);
		//리얼스토리 이미지 맞추기
		scollEvent();
	});

	$('.js-slide1').cycle({
		slides:'.js-slide1-item',
		fx:'scrollHorz',
		loop:false,
		timeout:0,
		prev:'.js-slide1-prev',
		next:'.js-slide1-next',
		pager:'.js-slide1-page',
		pagerTemplate	:'<span class="subway-map__bull">&bull;</span>',
		pagerActiveClass:'is-active',
		swipe:true
	});
	$('.js-slide2').cycle({
		slides:'.js-slide2-item',
		fx:'scrollHorz',
		loop:false,
		timeout:0,
		prev:'.js-slide2-prev',
		next:'.js-slide2-next',
		pager:'.js-slide2-page',
		pagerTemplate	:'<span class="subway-map__bull">&bull;</span>',
		pagerActiveClass:'is-active',
		swipe:true
	});
	var posY;
	$('.js-subway-open1').on('click',function(e){
		e.preventDefault();
		$('.js-type-1, .subway-dim').fadeIn();
		posY = $(window).scrollTop();
		$('html, body').addClass("is-active");
		$('.common').css("top",-posY);
	})
	$('.js-subway-open2').on('click',function(e){
		e.preventDefault();
		$('.js-type-2, .subway-dim').fadeIn();
		posY = $(window).scrollTop();
		$('html, body').addClass("is-active");
		$('.common').css("top",-posY);
	})
	$('.js-subway-close').on('click',function(e){
		e.preventDefault();
		$('.js-type-1, .js-type-2, .subway-dim').fadeOut();
		$('.common').css("top",'0');
		$('html, body').removeClass("is-active");
		posY = $(window).scrollTop(posY);
	});
	// google.maps.event.addDomListener(window, 'load', initMap);
});

// Google API
function initMap() {
  var myLatLng = {lat: 37.502660, lng: 127.035795};
  var centerLatLng = {lat : 37.502491, lng: 127.038728};
  var zoomSize = 15;

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('map'), {
	center: myLatLng,
	scrollwheel: false,
	zoom: 15
  });

  var iconBase = 'http://www.banobagi.com/img/common/';
  // Create a marker and set its position.
  var marker = new google.maps.Marker({
	map: map,
	position: myLatLng,
	icon: iconBase + 'banobagi_marker.png',
	size: new google.maps.Size(85, 52),
	title: 'Banobagi'
  });
}
