$(document).ready(function(){
	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 992) ? true : false;
				return r;
			}())
		);
		$('.main__section').css('height','100vh');
	});


	$(window).load(function(){
		$.scrollify({
			section : ".main__section",
			interstitialSection : ".footer",
			scrollSpeed: 700,
			scrollbars: false,
			touchScroll:true,
			setHeights:true,
			before: function(i,panels) {
	 			var ref = panels[i].attr("data-section-name");
				$(".pagination .active").removeClass("active");
      			$(".pagination").find("a[href=\"#" + ref + "\"]").addClass("active");
				if(ref==="section1") {
					$('.pagination').hide();
				}else{
					$('.pagination').show();
				}

				if(ref==="section2") {
					$(".js-img1").addClass("is-move");
					$(".js-img2, .js-img3, .js-img4, .js-img5").removeClass("is-move");
				}else if(ref==="section3") {
        			$(".js-img2").addClass("is-move");
					$(".js-img1, .js-img3, .js-img4, .js-img5").removeClass("is-move");
				}else if(ref==="section4"){
					$(".js-img3").addClass("is-move");
					$(".js-img1, .js-img2, .js-img4, .js-img5").removeClass("is-move");
				}else if(ref==="section5"){
					$(".js-img4").addClass("is-move");
					$(".js-img1, .js-img2, .js-img3, .js-img5").removeClass("is-move");
				}else if(ref==="section6"){
					$(".js-img5").addClass("is-move");
					$(".js-img1, .js-img2, .js-img3, .js-img4").removeClass("is-move");
				}else{
					$(".js-img1, .js-img2, .js-img3, .js-img4, .js-img5").removeClass("is-move");
				}
			},
			afterRender:function() {
		      var pagination = "<ul class=\"pagination\">";
		      var activeClass = "";
		      $(".page-section").each(function(i) {
		        activeClass = "";
		        if(i===0) {
		          activeClass = "active";
		        }
		        pagination += "<li class='pagination__item'><a class=\"" + activeClass + " pagination__link\" href=\"#" + $(this).attr("data-section-name") + "\"></a></li>";
		      });

		      pagination += "</ul>";

		      $(".main").append(pagination);
		      $(".pagination a").on("click",$.scrollify.move);
		  },afterResize:function() {
			  $.scrollify.update()
			  $('.main__section').css('height','100vh');
		  }
		});
		$('.js-down').on('click',function(){
			$.scrollify.move("#section2");
		});
		var cnt = 1;
		$('.js-menu').on('click',function(){
			if(cnt == 1){
				$.scrollify.disable();
				return cnt = 0;
			}else{
				$.scrollify.enable();
				return cnt = 1;
			}
		});
		$('.js-popup-open').on('click',function(){
			$.scrollify.disable();
		});
		$('.js-popup-close').on('click',function(){
			if(cnt == 0){
				return false;
			}
			$.scrollify.enable();
		});
	});
	$('.main__item').on('mouseenter',function(){
		if ($('body').attr('data-mobile') == 'false'){
			$(this).find('.main__link, .main__item-img').addClass('is-active');
		}
	}).on('mouseleave',function(){
		if ($('body').attr('data-mobile') == 'false'){
			$(this).find('.main__link, .main__item-img').removeClass('is-active');
		}
	});
	$('.main__item').on('click',function(){
		if ($('body').attr('data-mobile') == 'true'){
			$(this).find('.main__link, .main__item-img').toggleClass('is-active');
			$(this).siblings().find('.main__link, .main__item-img').removeClass('is-active');
		}
	})


	//유쿠 영상제어
	var playerCnt = 0;
	player = new YKU.Player('youkuplayer', {
		client_id: 'ff5a38840076b667',
		vid: 'XNDE2MDYwNzc4OA',
		 styleid: '0',
		autoplay: true,
		newPlayer: true,
		show_related: true
	});

	function onPlayEnd(){
		onPlayerStart();
	}
	function onPlayerStart(){
		player = new YKU.Player('youkuplayer', {
			client_id: 'ff5a38840076b667',
			vid: 'XNDE2MDYwNzc4OA',
			 styleid: '0',
			autoplay: true,
			newPlayer: true,
			show_related: true
		});
		playerCnt += 1;
	}

	var data;
	window.addEventListener('message',function(e){
	data = e.data;

	switch (data.msg){
			case "onPlayerStart":{
				//console.info(data.msg);
				onPlayerStart();
				break;
			}
			case "onPlayEnd":{
				//console.info(data.msg);/
				if(playerCnt < 2){
					onPlayEnd();
				}
				break;
			}
			/*
			case 'onTimeUpdate':{
				console.info(data.time);
				break;
			}*/
			case "state":{

			var state = data.stateParam;
			//console.info(state);
			break;
			}
		}
	},false);


	/* setCookie function */
	function setCookie(cname, value, expire) {
	   var todayValue = new Date();
	   // 오늘 날짜를 변수에 저장

	   todayValue.setDate(todayValue.getDate() + expire);
	   document.cookie = cname + "=" + encodeURI(value) + "; expires=" + todayValue.toGMTString() + "; path=/;";
	}
	// Get cookie function
	function getCookie(name) {
	   var cookieName = name + "=";
	   var x = 0;
	   while ( x <= document.cookie.length ) {
	      var y = (x+cookieName.length);
	      if ( document.cookie.substring( x, y ) == cookieName) {
	         if ((lastChrCookie=document.cookie.indexOf(";", y)) == -1)
	            lastChrCookie = document.cookie.length;
	         return decodeURI(document.cookie.substring(y, lastChrCookie));
	      }
	      x = document.cookie.indexOf(" ", x ) + 1;
	      if ( x == 0 )
	         break;
	      }
	   return "";
	}




	// 하루동안 안열기 쿠키 저장
	$(function() {
	   var closeTodayBtn = $('.js-popup-m-today');

	   // 버튼의 클래스명은 closeTodayBtn

	   closeTodayBtn.click(function(e) {
	      setCookie( "popup20180910", "end" , 1);
	      // 하루동안이므로 1을 설정
		  e.preventDefault();
		  $('.popup-m.type-first').removeClass('is-active')
	      // 현재 열려있는 팝업은 닫으면서 쿠키값을 저장
	   });
	});


	//쿠키체크후 팝업열기
	var result = getCookie('popup20180910');
	if (result != 'end') {
		$('.popup-m.type-first').addClass('is-active')
	}
	$('.js-popup-m-close').on('click',function(e){
		e.preventDefault();
		$(this).closest('.popup-m').removeClass('is-active')
	});

});
