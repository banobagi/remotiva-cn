
$(document).ready(function(){
	var isState = "P";
	var f;
	var url;
	f = $(".video__iframe");
	if(!f || !f.attr("src") ) return;
	url = f.attr("src").split("?")[0];
	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 992) ? true : false;
				return r;
			}())
		);
		var winH = $(window).height();
		var winW = $(window).width();
	});
	$.scrollify({
		section : ".swiper-slide",
		scrollbars: false,
		before: function(i,panels) {
			var ref = panels[i].attr("data-hash");
			if(ref==="gallery10") {
				var data = {
					method: "play",
					value: 1
				};
				f[0].contentWindow.postMessage(JSON.stringify(data), url);
			}else{
				var data = {
					method: "pause",
			        value: 1
			    };
			    f[0].contentWindow.postMessage(JSON.stringify(data), url);
			}
		}
	});
	$('.gallery-style1__tit, .gallery-style1__txt, .gallery-style1__subject, .gallery-circle, .gallery-style2__tit, .gallery-style2__move, .gallery-style2, .gallery-style3__move, .gallery-style4__tit, .gallery-style4__txt, .gallery-style5__tit, .gallery-style5__txt, .gallery-style6__move, .gallery-style6__tit, .gallery-style6, .gallery-style5__tit, .gallery-style5__txt, gallery-style5, .gallery-style7__tit, .gallery-style7__move, .gallery-style7__subject, .gallery-style7__box, .gallery-style1, .gallery-style2, .gallery-style4__tit, .gallery-style5').addClass('is-active');
	$('.gallery-style1__tit, .gallery-style1__txt, .gallery-style1__subject, .gallery-circle, .gallery-style2__tit, .gallery-style2__move, .gallery-style2, .gallery-style3__move, .gallery-style4__tit, .gallery-style4__txt, .gallery-style5__tit, .gallery-style5__txt, .gallery-style6__move, .gallery-style6__tit, .gallery-style6, .gallery-style5__tit, .gallery-style5__txt, gallery-style5, .gallery-style7__tit, .gallery-style7__move, .gallery-style7__subject, .gallery-style7__box, .gallery-style1, .gallery-style2, .gallery-style4__tit, .gallery-style5').addClass('animated');
	$('.gallery-style1__tit, .gallery-style1__txt, .gallery-style1__subject, .gallery-circle, .gallery-style2__tit, .gallery-style2__move, .gallery-style2, .gallery-style3__move, .gallery-style4__tit, .gallery-style4__txt, .gallery-style5__tit, .gallery-style5__txt, .gallery-style6__move, .gallery-style6__tit, .gallery-style6, .gallery-style5__tit, .gallery-style5__txt, gallery-style5, .gallery-style7__tit, .gallery-style7__move, .gallery-style7__subject, .gallery-style7__box, .gallery-style1, .gallery-style2, .gallery-style4__tit, .gallery-style5').addClass('fadeInUp');
	$('.gallery-style1__tit, .gallery-style1__txt, .gallery-style1__subject, .gallery-circle, .gallery-style2__tit, .gallery-style2__move, .gallery-style2, .gallery-style3__move, .gallery-style4__tit, .gallery-style4__txt, .gallery-style5__tit, .gallery-style5__txt, .gallery-style6__move, .gallery-style6__tit, .gallery-style6, .gallery-style5__tit, .gallery-style5__txt, gallery-style5, .gallery-style7__tit, .gallery-style7__move, .gallery-style7__subject, .gallery-style7__box, .gallery-style1, .gallery-style2, .gallery-style4__tit, .gallery-style5').css('opacity','1');
	$('.gallery-style1, .gallery-style6').addClass('is-9')
	$('.gallery__btn, .progress').hide();
	$('.gallery-page, .gallery-state').hide();


	$(document).on("click",".js-video-play",function(e){
		e.preventDefault();

		$('.video__bg, .video__btn').addClass('is-hide');
		var data = {
            method: "play",
            value: 1
        };
		f[0].contentWindow.postMessage(JSON.stringify(data), url);
	});
});
